from google.appengine.ext import ndb


class Message(ndb.Model):
    message = ndb.StringProperty()
    subject = ndb.StringProperty()
    sender = ndb.StringProperty()
    recipient = ndb.StringProperty()
    date = ndb.DateTimeProperty(auto_now_add=True)
    archived = ndb.BooleanProperty(default=False)
    replied_id = ndb.IntegerProperty()
    read = ndb.BooleanProperty(default=False)
