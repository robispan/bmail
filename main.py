#!/usr/bin/env python
import os
import jinja2
import webapp2
import time
from models import Message
from google.appengine.api import users

template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=False)


class BaseHandler(webapp2.RequestHandler):

    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        template = jinja_env.get_template(view_filename)
        self.response.out.write(template.render(params))


class MainHandler(BaseHandler):
    def get(self):
        user = users.get_current_user()
        if user:
            return self.redirect_to("inbox")
        else:
            logged_in = False
            login_url = users.create_login_url('/')

            params = {"logged_in": logged_in,
                      "login_url": login_url,
                      "user": user}

            return self.render_template("hello.html", params)


class InboxHandler(BaseHandler):
    def get(self):
        user = users.get_current_user()
        if user:
            logout_url = users.create_logout_url('/')

            # welcome message if new user
            received = Message.query(Message.recipient == user.email()).fetch()
            if not received:
                message = "Welcome to Bmail! Try sending a message to a friend who also uses Bmail."
                sender = "Bmail"
                recipient = user.email()
                subject = "Welcome"
                message = Message(message=message, sender=sender, recipient=recipient, subject=subject)
                message.put()
                time.sleep(0.1)

            # display inbox messages
            inbox = Message.query(Message.recipient == user.email(), Message.archived == False).fetch()
            inbox.sort(key=lambda r: r.date, reverse=True)

            params = {"inbox": inbox,
                      "logout_url": logout_url}

            return self.render_template("inbox.html", params=params)
        else:
            return self.redirect_to("main")


class SentHandler(BaseHandler):
    def get(self):
        user = users.get_current_user()

        if user:
            logout_url = users.create_logout_url('/')

            # display sent messages
            sent = Message.query(Message.sender == user.email(), Message.archived == False).fetch()
            sent.sort(key=lambda r: r.date, reverse=True)
            params = {"sent": sent,
                      "logout_url": logout_url}
            return self.render_template("sent.html", params=params)
        else:
            return self.redirect_to("main")


class NewMessageHandler(BaseHandler):

    def get(self):
        user = users.get_current_user()
        if user:
            logout_url = users.create_logout_url('/')
            params = {"logout_url": logout_url}
            return self.render_template("new.html", params=params)
        else:
            return self.redirect_to("main")

    def post(self):
        user = users.get_current_user()

        message = self.request.get("message")
        sender = user.email()
        recipient = self.request.get("recipient")
        subject = self.request.get("subject")
        if subject == "":
            subject = "No subject"

        message = Message(message=message, sender=sender, recipient=recipient, subject=subject)
        message.put()
        time.sleep(0.1)

        return self.redirect_to("inbox")


class MessageHandler(BaseHandler):
    def get(self, message_id):
        user = users.get_current_user()
        if user:
            logout_url = users.create_logout_url('/')
            message = Message.get_by_id(int(message_id))
            replied_message_id = message.replied_id
            # previous message is shown if available
            if replied_message_id:
                replied_message = Message.get_by_id(int(replied_message_id))
                params = {"message": message,
                          "replied_message": replied_message,
                          "logout_url": logout_url}
            else:
                params = {"message": message,
                          "logout_url": logout_url}
            # if message is opened for the first time it is marked as read
            if not message.read:
                message.read = True
                message.put()
            return self.render_template("message.html", params=params)
        else:
            return self.redirect_to("main")


class ReplyMessageHandler(BaseHandler):
    # get method renders reply.html with preconfigured To and Subject fields (collected from message_id argument)
    def get(self, message_id):
        user = users.get_current_user()
        if user:
            logout_url = users.create_logout_url('/')
            replied_message = Message.get_by_id(int(message_id))  # message to which user wants to reply
            recipient = replied_message.sender
            subject = "Re: " + replied_message.subject

            params = {"recipient": recipient,
                      "subject": subject,
                      "replied_message": replied_message,
                      "logout_url": logout_url}

            return self.render_template("reply.html", params=params)
        else:
            return self.redirect_to("main")

    # post method adds reply message to database with added id of previous message
    def post(self, message_id):
        user = users.get_current_user()
        if user:
            message = self.request.get("message")
            sender = user.email()
            recipient = self.request.get("recipient")
            subject = self.request.get("subject")
            replied_id = Message.get_by_id(int(message_id)).key.id()

            message = Message(message=message,
                              sender=sender,
                              recipient=recipient,
                              subject=subject,
                              replied_id=replied_id)
            message.put()

            return self.redirect_to("inbox")
        else:
            return self.redirect_to("main")


class DeleteHandler(BaseHandler):
    def post(self, deleted_id):
        user = users.get_current_user()
        if user:
            logout_url = users.create_logout_url('/')
            deleted_message = Message.get_by_id(int(deleted_id))
            undo = self.request.get("undo")
            if undo:
                # if user clicked undo, parameter "undo" is posted with value True and message is undeleted
                deleted_message.archived = False
                deleted_message.put()
                time.sleep(0.1)
                if deleted_message.recipient == user.email():
                    return self.redirect_to("inbox")
                elif deleted_message.sender == user.email():
                    return self.redirect_to("sent")
            else:
                # if no "undo" parameter is posted then message is soft deleted and undo button is shown
                deleted_message.archived = True
                deleted_message.put()
                time.sleep(0.1)
                if deleted_message.recipient == user.email():
                    inbox = Message.query(Message.recipient == user.email(), Message.archived == False).fetch()
                    inbox.sort(key=lambda r: r.date, reverse=True)
                    params = {"inbox": inbox,
                              "deleted_message": deleted_message,
                              "logout_url": logout_url}
                    return self.render_template("inbox.html", params=params)
                elif deleted_message.sender == user.email():
                    sent = Message.query(Message.sender == user.email(), Message.archived == False).fetch()
                    sent.sort(key=lambda r: r.date, reverse=True)
                    params = {"sent": sent,
                              "deleted_message": deleted_message,
                              "logout_url": logout_url}
                    return self.render_template("sent.html", params=params)
        else:
            return self.redirect_to("main")


app = webapp2.WSGIApplication([
    webapp2.Route('/', MainHandler, name="main"),
    webapp2.Route('/inbox', InboxHandler, name="inbox"),
    webapp2.Route('/sent', SentHandler, name="sent"),
    webapp2.Route('/new', NewMessageHandler, name="new"),
    webapp2.Route('/message/<message_id:\d+>', MessageHandler),
    webapp2.Route('/reply/<message_id:\d+>', ReplyMessageHandler),
    webapp2.Route('/delete/<deleted_id:\d+>', DeleteHandler),
], debug=True)
